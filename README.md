## rospeex (jade) - 3.0.1-0

The packages in the `rospeex` repository were released into the `jade` distro by running `/usr/bin/bloom-release --rosdistro jade --track jade rospeex` on `Thu, 20 Apr 2017 03:05:12 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:

- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `3.0.0-0`
- old version: `3.0.0-0`
- new version: `3.0.1-0`

Versions of tools used:

- bloom version: `0.5.23`
- catkin_pkg version: `0.3.1`
- rosdep version: `0.11.5`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.38`


## rospeex (indigo) - 3.0.1-1

The packages in the `rospeex` repository were released into the `indigo` distro by running `/usr/bin/bloom-release --rosdistro indigo --track indigo rospeex` on `Wed, 19 Apr 2017 09:24:05 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:

- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `3.0.0-0`
- old version: `3.0.1-0`
- new version: `3.0.1-1`

Versions of tools used:

- bloom version: `0.5.23`
- catkin_pkg version: `0.3.1`
- rosdep version: `0.11.5`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.38`


## rospeex (indigo) - 3.0.1-0

The packages in the `rospeex` repository were released into the `indigo` distro by running `/usr/bin/bloom-release --rosdistro indigo --track indigo rospeex` on `Wed, 19 Apr 2017 09:17:40 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:

- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `3.0.0-0`
- old version: `3.0.0-0`
- new version: `3.0.1-0`

Versions of tools used:

- bloom version: `0.5.23`
- catkin_pkg version: `0.3.1`
- rosdep version: `0.11.5`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.38`


## rospeex (jade) - 3.0.0-0

The packages in the `rospeex` repository were released into the `jade` distro by running `/usr/bin/bloom-release --rosdistro jade --track jade rospeex` on `Wed, 05 Apr 2017 08:46:44 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:

- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.15.4-0`
- old version: `2.15.4-0`
- new version: `3.0.0-0`

Versions of tools used:

- bloom version: `0.5.23`
- catkin_pkg version: `0.3.1`
- rosdep version: `0.11.5`
- rosdistro version: `0.6.1`
- vcstools version: `0.1.38`


## rospeex (indigo) - 3.0.0-0

The packages in the `rospeex` repository were released into the `indigo` distro by running `/usr/bin/bloom-release --rosdistro indigo --track indigo rospeex` on `Wed, 05 Apr 2017 06:51:08 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:

- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.15.4-0`
- old version: `2.15.4-0`
- new version: `3.0.0-0`

Versions of tools used:

- bloom version: `0.5.23`
- catkin_pkg version: `0.3.1`
- rosdep version: `0.11.5`
- rosdistro version: `0.6.1`
- vcstools version: `0.1.38`


## rospeex (jade) - 2.15.4-0

The packages in the `rospeex` repository were released into the `jade` distro by running `/usr/bin/bloom-release --rosdistro jade --track jade rospeex` on `Thu, 09 Feb 2017 06:03:47 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:

- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.15.2-0`
- old version: `2.15.2-0`
- new version: `2.15.4-0`

Versions of tools used:

- bloom version: `0.5.23`
- catkin_pkg version: `0.2.10`
- rosdep version: `0.11.5`
- rosdistro version: `0.5.0`
- vcstools version: `0.1.38`


## rospeex (indigo) - 2.15.4-0

The packages in the `rospeex` repository were released into the `indigo` distro by running `/usr/bin/bloom-release --rosdistro indigo --track indigo rospeex` on `Thu, 09 Feb 2017 05:11:44 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:

- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.15.3-0`
- old version: `2.15.3-0`
- new version: `2.15.4-0`

Versions of tools used:

- bloom version: `0.5.23`
- catkin_pkg version: `0.2.10`
- rosdep version: `0.11.5`
- rosdistro version: `0.5.0`
- vcstools version: `0.1.38`


## rospeex (indigo) - 2.15.3-0

The packages in the `rospeex` repository were released into the `indigo` distro by running `/usr/bin/bloom-release --rosdistro indigo --track indigo rospeex` on `Wed, 01 Feb 2017 08:16:06 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:

- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.15.2-0`
- old version: `2.15.2-0`
- new version: `2.15.3-0`

Versions of tools used:

- bloom version: `0.5.23`
- catkin_pkg version: `0.2.10`
- rosdep version: `0.11.5`
- rosdistro version: `0.5.0`
- vcstools version: `0.1.38`


## rospeex (jade) - 2.15.2-0

The packages in the `rospeex` repository were released into the `jade` distro by running `/usr/bin/bloom-release --rosdistro jade --track jade rospeex` on `Mon, 16 Jan 2017 03:45:22 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:

- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.15.0-0`
- old version: `2.15.0-0`
- new version: `2.15.2-0`

Versions of tools used:

- bloom version: `0.5.23`
- catkin_pkg version: `0.2.10`
- rosdep version: `0.11.5`
- rosdistro version: `0.5.0`
- vcstools version: `0.1.38`


## rospeex (indigo) - 2.15.2-0

The packages in the `rospeex` repository were released into the `indigo` distro by running `/usr/bin/bloom-release --rosdistro indigo --track indigo rospeex` on `Mon, 16 Jan 2017 03:21:02 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:

- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.15.0-0`
- old version: `2.15.0-0`
- new version: `2.15.2-0`

Versions of tools used:

- bloom version: `0.5.23`
- catkin_pkg version: `0.2.10`
- rosdep version: `0.11.5`
- rosdistro version: `0.5.0`
- vcstools version: `0.1.38`


## rospeex (jade) - 2.15.0-0

The packages in the `rospeex` repository were released into the `jade` distro by running `/usr/bin/bloom-release --rosdistro jade --track jade rospeex` on `Wed, 07 Dec 2016 03:05:01 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:

- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.14.6-0`
- old version: `2.14.6-0`
- new version: `2.15.0-0`

Versions of tools used:

- bloom version: `0.5.23`
- catkin_pkg version: `0.2.10`
- rosdep version: `0.11.5`
- rosdistro version: `0.4.2`
- vcstools version: `0.1.38`


## rospeex (indigo) - 2.15.0-0

The packages in the `rospeex` repository were released into the `indigo` distro by running `/usr/bin/bloom-release --rosdistro indigo --track indigo rospeex` on `Wed, 07 Dec 2016 02:54:27 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:

- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.14.7-0`
- old version: `2.14.7-0`
- new version: `2.15.0-0`

Versions of tools used:

- bloom version: `0.5.23`
- catkin_pkg version: `0.2.10`
- rosdep version: `0.11.5`
- rosdistro version: `0.4.2`
- vcstools version: `0.1.38`


## rospeex (indigo) - 2.14.7-0

The packages in the `rospeex` repository were released into the `indigo` distro by running `/usr/bin/bloom-release --rosdistro indigo --track indigo rospeex` on `Thu, 31 Mar 2016 05:28:15 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.14.6-1`
- old version: `2.14.6-1`
- new version: `2.14.7-0`

Versions of tools used:
- bloom version: `0.5.20`
- catkin_pkg version: `0.2.10`
- rosdep version: `0.11.4`
- rosdistro version: `0.4.2`
- vcstools version: `0.1.38`


## rospeex (jade) - 2.14.6-0

The packages in the `rospeex` repository were released into the `jade` distro by running `/usr/bin/bloom-release --rosdistro jade --track jade rospeex` on `Thu, 14 Jan 2016 07:38:03 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.14.4-0`
- old version: `2.14.4-0`
- new version: `2.14.6-0`

Versions of tools used:
- bloom version: `0.5.20`
- catkin_pkg version: `0.2.10`
- rosdep version: `0.11.4`
- rosdistro version: `0.4.2`
- vcstools version: `0.1.38`


## rospeex (indigo) - 2.14.6-1

The packages in the `rospeex` repository were released into the `indigo` distro by running `/usr/bin/bloom-release --rosdistro indigo --track indigo rospeex` on `Thu, 14 Jan 2016 05:41:30 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.14.4-0`
- old version: `2.14.6-0`
- new version: `2.14.6-1`

Versions of tools used:
- bloom version: `0.5.20`
- catkin_pkg version: `0.2.10`
- rosdep version: `0.11.4`
- rosdistro version: `0.4.2`
- vcstools version: `0.1.38`


## rospeex (indigo) - 2.14.6-0

The packages in the `rospeex` repository were released into the `indigo` distro by running `/usr/bin/bloom-release --rosdistro indigo --track indigo rospeex` on `Thu, 14 Jan 2016 05:04:42 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.14.4-0`
- old version: `2.14.4-0`
- new version: `2.14.6-0`

Versions of tools used:
- bloom version: `0.5.20`
- catkin_pkg version: `0.2.10`
- rosdep version: `0.11.4`
- rosdistro version: `0.4.2`
- vcstools version: `0.1.38`


## rospeex (jade) - 2.14.4-0

The packages in the `rospeex` repository were released into the `jade` distro by running `/usr/bin/bloom-release rospeex --track jade --rosdistro jade --new-track` on `Wed, 02 Dec 2015 01:50:32 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `2.14.4-0`

Versions of tools used:
- bloom version: `0.5.20`
- catkin_pkg version: `0.2.10`
- rosdep version: `0.11.4`
- rosdistro version: `0.4.2`
- vcstools version: `0.1.38`


## rospeex (indigo) - 2.14.4-0

The packages in the `rospeex` repository were released into the `indigo` distro by running `/usr/bin/bloom-release --rosdistro indigo --track indigo rospeex` on `Tue, 01 Dec 2015 10:51:54 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.14.3-0`
- old version: `2.14.3-0`
- new version: `2.14.4-0`

Versions of tools used:
- bloom version: `0.5.20`
- catkin_pkg version: `0.2.10`
- rosdep version: `0.11.4`
- rosdistro version: `0.4.2`
- vcstools version: `0.1.38`


## rospeex (indigo) - 2.14.3-0

The packages in the `rospeex` repository were released into the `indigo` distro by running `/usr/bin/bloom-release --rosdistro indigo --track indigo rospeex` on `Tue, 01 Dec 2015 04:57:42 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.14.2-2`
- old version: `2.14.2-2`
- new version: `2.14.3-0`

Versions of tools used:
- bloom version: `0.5.20`
- catkin_pkg version: `0.2.10`
- rosdep version: `0.11.4`
- rosdistro version: `0.4.2`
- vcstools version: `0.1.38`


## rospeex (indigo) - 2.14.2-2

The packages in the `rospeex` repository were released into the `indigo` distro by running `/usr/bin/bloom-release --rosdistro indigo --track indigo rospeex` on `Fri, 27 Nov 2015 02:13:27 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.14.2-1`
- old version: `2.14.2-1`
- new version: `2.14.2-2`

Versions of tools used:
- bloom version: `0.5.20`
- catkin_pkg version: `0.2.10`
- rosdep version: `0.11.4`
- rosdistro version: `0.4.2`
- vcstools version: `0.1.38`


## rospeex (indigo) - 2.14.2-1

The packages in the `rospeex` repository were released into the `indigo` distro by running `/usr/bin/bloom-release --rosdistro indigo --track indigo rospeex` on `Thu, 26 Nov 2015 07:31:15 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.14.2-0`
- old version: `2.14.2-0`
- new version: `2.14.2-1`

Versions of tools used:
- bloom version: `0.5.20`
- catkin_pkg version: `0.2.10`
- rosdep version: `0.11.4`
- rosdistro version: `0.4.2`
- vcstools version: `0.1.38`


## rospeex (indigo) - 2.14.2-0

The packages in the `rospeex` repository were released into the `indigo` distro by running `/usr/bin/bloom-release --rosdistro indigo --track indigo rospeex` on `Thu, 26 Nov 2015 05:26:00 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.14.1-0`
- old version: `2.14.1-0`
- new version: `2.14.2-0`

Versions of tools used:
- bloom version: `0.5.20`
- catkin_pkg version: `0.2.10`
- rosdep version: `0.11.4`
- rosdistro version: `0.4.2`
- vcstools version: `0.1.38`


## rospeex (indigo) - 2.14.1-0

The packages in the `rospeex` repository were released into the `indigo` distro by running `/usr/bin/bloom-release --rosdistro indigo --track indigo rospeex` on `Fri, 18 Sep 2015 08:31:32 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.14.0-2`
- old version: `2.14.0-2`
- new version: `2.14.1-0`

Versions of tools used:
- bloom version: `0.5.20`
- catkin_pkg version: `0.2.10`
- rosdep version: `0.11.2`
- rosdistro version: `0.4.2`
- vcstools version: `0.1.37`


## rospeex (indigo) - 2.14.0-2

The packages in the `rospeex` repository were released into the `indigo` distro by running `/usr/bin/bloom-release --rosdistro indigo --track indigo rospeex` on `Fri, 18 Sep 2015 05:08:42 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.12.6-0`
- old version: `2.14.0-1`
- new version: `2.14.0-2`

Versions of tools used:
- bloom version: `0.5.20`
- catkin_pkg version: `0.2.10`
- rosdep version: `0.11.2`
- rosdistro version: `0.4.2`
- vcstools version: `0.1.37`


## rospeex (indigo) - 2.14.0-1

The packages in the `rospeex` repository were released into the `indigo` distro by running `/usr/bin/bloom-release --rosdistro indigo --track indigo rospeex` on `Fri, 18 Sep 2015 04:39:37 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.12.6-0`
- old version: `2.14.0-0`
- new version: `2.14.0-1`

Versions of tools used:
- bloom version: `0.5.20`
- catkin_pkg version: `0.2.10`
- rosdep version: `0.11.2`
- rosdistro version: `0.4.2`
- vcstools version: `0.1.37`


## rospeex (indigo) - 2.14.0-0

The packages in the `rospeex` repository were released into the `indigo` distro by running `/usr/bin/bloom-release --rosdistro indigo --track indigo rospeex` on `Fri, 18 Sep 2015 04:16:23 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.12.6-0`
- old version: `2.12.6-0`
- new version: `2.14.0-0`

Versions of tools used:
- bloom version: `0.5.20`
- catkin_pkg version: `0.2.10`
- rosdep version: `0.11.2`
- rosdistro version: `0.4.2`
- vcstools version: `0.1.37`


## rospeex (indigo) - 2.12.6-0

The packages in the `rospeex` repository were released into the `indigo` distro by running `/usr/bin/bloom-release rospeex --track indigo --rosdistro indigo --new-track` on `Fri, 24 Apr 2015 01:56:36 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `2.12.6-0`

Versions of tools used:
- bloom version: `0.5.20`
- catkin_pkg version: `0.2.8`
- rosdep version: `0.11.2`
- rosdistro version: `0.4.2`
- vcstools version: `0.1.36`


## rospeex (hydro) - 2.12.5-0

The packages in the `rospeex` repository were released into the `hydro` distro by running `/usr/bin/bloom-release --rosdistro hydro --track hydro rospeex` on `Tue, 03 Mar 2015 05:19:28 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.12.4-0`
- old version: `2.12.4-0`
- new version: `2.12.5-0`

Versions of tools used:
- bloom version: `0.5.16`
- catkin_pkg version: `0.2.6`
- rosdep version: `0.10.33`
- rosdistro version: `0.4.0`
- vcstools version: `0.1.35`


## rospeex (hydro) - 2.12.4-0

The packages in the `rospeex` repository were released into the `hydro` distro by running `/usr/bin/bloom-release --rosdistro hydro --track hydro rospeex` on `Thu, 19 Feb 2015 05:40:32 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.12.3-1`
- old version: `2.12.3-1`
- new version: `2.12.4-0`

Versions of tools used:
- bloom version: `0.5.16`
- catkin_pkg version: `0.2.6`
- rosdep version: `0.10.33`
- rosdistro version: `0.4.0`
- vcstools version: `0.1.35`


## rospeex (hydro) - 2.12.3-1

The packages in the `rospeex` repository were released into the `hydro` distro by running `/usr/bin/bloom-release --rosdistro hydro --track hydro rospeex` on `Thu, 19 Feb 2015 02:56:49 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.12.3-0`
- old version: `2.12.3-0`
- new version: `2.12.3-1`

Versions of tools used:
- bloom version: `0.5.16`
- catkin_pkg version: `0.2.6`
- rosdep version: `0.10.33`
- rosdistro version: `0.4.0`
- vcstools version: `0.1.35`


## rospeex (hydro) - 2.12.3-0

The packages in the `rospeex` repository were released into the `hydro` distro by running `/usr/bin/bloom-release --rosdistro hydro --track hydro rospeex --edit` on `Fri, 13 Feb 2015 04:31:37 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.12.2-0`
- old version: `2.12.2-0`
- new version: `2.12.3-0`

Versions of tools used:
- bloom version: `0.5.16`
- catkin_pkg version: `0.2.6`
- rosdep version: `0.10.33`
- rosdistro version: `0.4.0`
- vcstools version: `0.1.35`


## rospeex (hydro) - 2.12.2-0

The packages in the `rospeex` repository were released into the `hydro` distro by running `/usr/bin/bloom-release --rosdistro hydro --track hydro rospeex` on `Thu, 08 Jan 2015 04:33:43 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.12.1-0`
- old version: `2.12.1-1`
- new version: `2.12.2-0`

Versions of tools used:
- bloom version: `0.5.16`
- catkin_pkg version: `0.2.6`
- rosdep version: `0.10.33`
- rosdistro version: `0.4.0`
- vcstools version: `0.1.35`


## rospeex (hydro) - 2.12.1-1

The packages in the `rospeex` repository were released into the `hydro` distro by running `/usr/bin/bloom-release --rosdistro hydro --track hydro rospeex` on `Thu, 08 Jan 2015 01:25:33 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.12.1-0`
- old version: `2.12.1-0`
- new version: `2.12.1-1`

Versions of tools used:
- bloom version: `0.5.16`
- catkin_pkg version: `0.2.6`
- rosdep version: `0.10.33`
- rosdistro version: `0.4.0`
- vcstools version: `0.1.35`


## rospeex (hydro) - 2.12.1-0

The packages in the `rospeex` repository were released into the `hydro` distro by running `/usr/bin/bloom-release --rosdistro hydro --track hydro rospeex` on `Wed, 07 Jan 2015 09:08:51 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: https://bitbucket.org/rospeex/rospeex-release.git
- rosdistro version: `2.12.0-0`
- old version: `2.12.0-0`
- new version: `2.12.1-0`

Versions of tools used:
- bloom version: `0.5.16`
- catkin_pkg version: `0.2.6`
- rosdep version: `0.10.33`
- rosdistro version: `0.4.0`
- vcstools version: `0.1.35`


## rospeex (hydro) - 2.12.0-0

The packages in the `rospeex` repository were released into the `hydro` distro by running `/usr/bin/bloom-release --rosdistro hydro --track hydro rospeex --edit` on `Fri, 26 Dec 2014 06:39:14 -0000`

These packages were released:
- `rospeex`
- `rospeex_audiomonitor`
- `rospeex_core`
- `rospeex_if`
- `rospeex_launch`
- `rospeex_msgs`
- `rospeex_samples`
- `rospeex_webaudiomonitor`

Version of package(s) in repository `rospeex`:
- upstream repository: https://bitbucket.org/rospeex/rospeex.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `2.12.0-0`

Versions of tools used:
- bloom version: `0.5.16`
- catkin_pkg version: `0.2.6`
- rosdep version: `0.10.33`
- rosdistro version: `0.4.0`
- vcstools version: `0.1.35`


